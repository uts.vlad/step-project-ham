//блок задания переменных дублированных в CSS
const body = document.getElementsByTagName('body')[0];
body.style.setProperty('--companyGreen', '#18C1AB');
body.style.setProperty('companyBlue', '#00CBF6');
body.style.setProperty('wrapper_width',1160+'px');
let companyGreen = body.style.getPropertyValue('--companyGreen');

//блок обработки раздела SERVICES
const servicesTabs = document.querySelector('.services-tabs');
const servicesContent = document.querySelectorAll('.services-content');
for (let elem of servicesContent) elem.style.width = `${servicesTabs.clientWidth}px`;

let oldServicesContent = document.querySelector(`.services-content`);
oldServicesContent.style.display = "flex"; //выбрали первый элемент и сделали его "видимым" по-умолчанию при загрузке
const showContent=(e)=>{
    if ( oldServicesContent ) oldServicesContent.style.display = "none";// скрыли ранее включенный блок
    const currentServicesContentBox = document.querySelector(`div[data-name = "${e.target.innerText}"]`);
    currentServicesContentBox.style.display = "flex" ;
    oldServicesContent = currentServicesContentBox;

};
servicesTabs.addEventListener("click", showContent);
//конец блока обработки раздела SERVICES-------------------

const ourworksPictures=[
    /*default -7шт*/
    {
        status: "default",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/pics on page/1.png",
        project: "someproject1",
    },    {
        status: "default",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/pics on page/2.png",
        project: "someproject1",
    },    {
        status: "default",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/pics on page/3.png",
        project: "someproject1",
    },    {
        status: "default",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/pics on page/4.png",
        project: "someproject2",
    },    {
        status: "default",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/pics on page/5.png",
        project: "someproject3",
    },    {
        status: "default",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/pics on page/6.png",
        project: "someproject5",
    },
    {
        status: "default",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/pics on page/7.png",
        project: "someproject3",
    },
    {
        status: "default",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/pics on page/8.png",
        project: "someproject5",
    },

    /*grafic design  -12шт*/
    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design1.jpg",
        project: "someproject4",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design2.jpg",
        project: "someproject4",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design3.jpg",
        project: "someproject4",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design4.jpg",
        project: "someproject4",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design5.jpg",
        project: "someproject4",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design6.jpg",
        project: "someproject4",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design7.jpg",
        project: "someproject7",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design8.jpg",
        project: "someproject7",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design9.jpg",
        project: "someproject7",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design10.jpg",
        project: "someproject7",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design11.jpg",
        project: "someproject7",
    },    {
        status: "general",
        theme: "Graphic Design",
        fileLocation: "pictures/amazing-works/graphic design/graphic-design12.jpg",
        project: "someproject7",
    },


/*web design  -7шт*/
    {
        status: "general",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/web design/web-design1.jpg",
        project: "someproject8",
    },
    {
        status: "general",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/web design/web-design2.jpg",
        project: "someproject8",
    },
    {
        status: "general",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/web design/web-design3.jpg",
        project: "someproject8",
    },
    {
        status: "general",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/web design/web-design4.jpg",
        project: "someproject8",
    },
    {
        status: "general",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/web design/web-design5.jpg",
        project: "someproject8",
    },
    {
        status: "general",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/web design/web-design6.jpg",
        project: "someproject8",
    },
    {
        status: "general",
        theme: "Web Design",
        fileLocation: "pictures/amazing-works/web design/web-design7.jpg",
        project: "someproject8",
    },


    /*wordpress  -10шт*/
    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress1.jpg",
        project: "someproject9",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress2.jpg",
        project: "someproject9",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress3.jpg",

        project: "someproject9",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress4.jpg",

        project: "someproject9",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress5.jpg",
        project: "someproject25",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress6.jpg",
        project: "someproject25",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress7.jpg",
        project: "someproject25",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress8.jpg",
        project: "someproject25",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress9.jpg",
        project: "someproject15",
    },    {
        status: "general",
        theme: "Wordpress",
        fileLocation: "pictures/amazing-works/wordpress/wordpress10.jpg",
        project: "someproject15",
    },

    /*landing pages -7шт*/
    {
        status: "general",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/landing page/landing-page1.jpg",
        project: "someproject15",
    },

       {
        status: "general",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/landing page/landing-page2.jpg",
        project: "someproject15",
    },

       {
        status: "general",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/landing page/landing-page3.jpg",
        project: "someproject15",
    },

       {
        status: "general",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/landing page/landing-page4.jpg",
        project: "someproject15",
    },

       {
        status: "general",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/landing page/landing-page5.jpg",
        project: "someproject15",
    },

       {
        status: "general",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/landing page/landing-page6.jpg",
    },

       {
        status: "general",
        theme: "Landing Pages",
        fileLocation: "pictures/amazing-works/landing page/landing-page7.jpg",
    },
];



//блок обработки раздела OUR AMAZING WORKS ------------------------------------------------
const filterOffPics =(dataArr, requestedTheme = "all", requestedStatus = "default")=>{
    // debugger
    return dataArr.filter((elem)=>{
        if ( elem.status.toLowerCase() === requestedStatus.toLowerCase() )
            if ( requestedTheme.toLowerCase() === "all" || elem.theme.toLowerCase()=== requestedTheme.toLowerCase() ) return true
    })
};




//блок вывода в DOM дефолтного набора картинок и их "фильтрация" с помощь ТАБов  раздела our works
let picContainer = document.createElement('div'); //сюда собираю контейнер вывода изображений на рендеринг
picContainer.className = "amazing-work-container";
const loadMoreButton = document.getElementById("our-works-button");

const pictureToOut=(theme, status)=>{
    const picturesToPage = filterOffPics(ourworksPictures, theme, status);
/*if (picContainer === undefined) {picContainer = document.createElement('div');
    picContainer.className = "amazing-work-container"}
else */picContainer.innerHTML = "";
 // let containerContent=[];
    for (let i=0; i < picturesToPage.length; i++) {
        picContainer.innerHTML += `<div data-name='${picturesToPage[i].theme}' data-project='${picturesToPage[i].project}' class="contentDIV">
        <img src="${picturesToPage[i].fileLocation}" alt="our works">
        </div>\n`;
 // а можно выпендриться и через сборку строки через прогонку в массиве:
 //        containerContent.push(`<div data-name='${picturesToPage[i].theme}' class="our-amazing-works-content">
 //        <img src="${picturesToPage[i].fileLocation}" alt="our works">
 //        </div>\n`);  //получили массив с HTML кодом, который в след.строке запихнем в строку по .join():
    }//for
    // picContainer.innerHTML = containerContent.join("");
    loadMoreButton.before(picContainer);
}; //pictureToOut()
pictureToOut("all", "default"); //сразу и выводим дефолтные картинки при первом рендеринге страницы.





//обработчик нажатия ТАБов фильтрации дефолтного блока картинок раздела our works
let globalTheme;
const tabsPressHandler=(e)=>{
//ВНИМАНИЕ:  САМЫЕ надежные 2 способа удаления группы узлов -это либо возвращающий
// живую коллекцию  getElementsByClassName:
/*    while (document.getElementsByClassName('amazing-work-container')[0]){
        document.getElementsByClassName('amazing-work-container')[0].remove();
    }*/
// либо если применяю возвращающий "неживую" коллекцию: querySelectorAll, то так:
//     const container = document.querySelectorAll('.amazing-work-container');
//     for (let i=0; i < container.length; i++) container[i].remove();
    let TabTarget = e.target;
    let tabsChildren = Array.from(TabTarget.closest('ul').children);
    tabsChildren.forEach((el)=>{el.style.color = '#717171'});
    TabTarget.style.color = "#717171";
    TabTarget.style.color = companyGreen;
    TabTarget.style.backgroundColor = "white";
    pictureToOut(TabTarget.innerText, "default");
    loadMoreButton.style.visibility = "visible"; //снова включаем кнопку "+load more"
    globalTheme = TabTarget.innerText;
};




//блок формирования и вывода в DOM дополнительных групп по максимум 12-картинок по нажатию кнопки "+load more"


const showLoadingMSG=()=> {
    const loadingMSG = document.createElement('div');
    loadingMSG.innerText = "данные загружаются с сервера!!!";
    loadingMSG.id= "loadingMSG";
    loadMoreButton.before(loadingMSG);
};
const deleteLoadingMSG=()=> document.getElementById('loadingMSG').remove();

const pushMorePicsToPage=()=> {
    showLoadingMSG();
    const tm1 = setTimeout( ()=>{ deleteLoadingMSG(); addMoreItems()  }, 2000);
};

let nextTwelveItems =0;
const addMoreItems = () => {

    const picturesToPage = filterOffPics(ourworksPictures, globalTheme, "general"); //теперь выборку ведем только по картинкам "general" (default уже выведены ранее)
    if (nextTwelveItems >= picturesToPage.length ){ nextTwelveItems = 0; loadMoreButton.style.visibility = "hidden"; return}  //резервная строка остановки
    let iteratorMin = nextTwelveItems, iteratorMax;
    let itemsRemain = picturesToPage.length - nextTwelveItems;
    if (itemsRemain <= 0) {loadMoreButton.style.visibility = "hidden";  nextTwelveItems = 0; return  }
    else if (itemsRemain >= 12) {iteratorMax = iteratorMin +12; nextTwelveItems += 12}
    else { iteratorMax = picturesToPage.length; nextTwelveItems += ( iteratorMax - iteratorMin )}

    for (let i= iteratorMin; i < iteratorMax; i++) {
        picContainer.innerHTML += `<div data-name='${picturesToPage[i].theme}' data-project='${picturesToPage[i].project}' class="contentDIV">
        <img src="${picturesToPage[i].fileLocation}" alt="our works">
        </div>\n`;
    }
    if (nextTwelveItems >= picturesToPage.length ){ nextTwelveItems = 0; loadMoreButton.style.visibility = "hidden"} //основная строка остановки
};

let elem = null; //очистится эта переменная уже только аж в обработчике mouseout
const processContentOnHover=(e)=>{ //важно как писать этот обработчик:  https://learn.javascript.ru/mousemove-mouseover-mouseout-mouseenter-mouseleave
// перед тем, как войти на следующий элемент, курсор всегда покидает предыдущий
// если elem ещё существует (т.е. не null), то мы ещё не ушли с предыдущего элемента. Важно ОТФИЛЬТРОВАТЬ любые
// переходы НЕ на ЕДИНСТВЕННЫЙ интересующий нас элемент (в нашем случае -это <div> class="our-amazing-works-content" )
    if (elem) return; // это переход внутри - игнорируем такое событие
    if ( e.target.closest('div').dataset.name ) //только, когда добрались по событию до элем.содержащего атрибут присутствующий в искомом элементе, то:
        elem = e.target.closest('div'); else return;
    const theme = elem.dataset.name;
    const project = elem.dataset.project;
    const ourImage = elem.querySelector('img');
    if (ourImage) ourImage.style.opacity = "0"; //скрываем изображения, оставляем только div
    const elemNewFace = document.createElement('div');
    elemNewFace.className ="elemNewFace";
    elemNewFace.innerHTML = `<a href='#'><svg class="link-svg" width="20px" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M326.612 185.391c59.747 59.809 58.927 155.698.36 214.59-.11.12-.24.25-.36.37l-67.2 67.2c-59.27 59.27-155.699 59.262-214.96 0-59.27-59.26-59.27-155.7 0-214.96l37.106-37.106c9.84-9.84 26.786-3.3 27.294 10.606.648 17.722 3.826 35.527 9.69 52.721 1.986 5.822.567 12.262-3.783 16.612l-13.087 13.087c-28.026 28.026-28.905 73.66-1.155 101.96 28.024 28.579 74.086 28.749 102.325.51l67.2-67.19c28.191-28.191 28.073-73.757 0-101.83-3.701-3.694-7.429-6.564-10.341-8.569a16.037 16.037 0 0 1-6.947-12.606c-.396-10.567 3.348-21.456 11.698-29.806l21.054-21.055c5.521-5.521 14.182-6.199 20.584-1.731a152.482 152.482 0 0 1 20.522 17.197zM467.547 44.449c-59.261-59.262-155.69-59.27-214.96 0l-67.2 67.2c-.12.12-.25.25-.36.37-58.566 58.892-59.387 154.781.36 214.59a152.454 152.454 0 0 0 20.521 17.196c6.402 4.468 15.064 3.789 20.584-1.731l21.054-21.055c8.35-8.35 12.094-19.239 11.698-29.806a16.037 16.037 0 0 0-6.947-12.606c-2.912-2.005-6.64-4.875-10.341-8.569-28.073-28.073-28.191-73.639 0-101.83l67.2-67.19c28.239-28.239 74.3-28.069 102.325.51 27.75 28.3 26.872 73.934-1.155 101.96l-13.087 13.087c-4.35 4.35-5.769 10.79-3.783 16.612 5.864 17.194 9.042 34.999 9.69 52.721.509 13.906 17.454 20.446 27.294 10.606l37.106-37.106c59.271-59.259 59.271-155.699.001-214.959z"/></svg></a><div class="square"></div><p class='elem-project'>${project}</p><p class='elem-theme'>${theme}</p>`;
    elem.append(elemNewFace);
};

const deleteContentOnHover=(e)=>{
    if (elem === null) return; //если уже был обработан mouseout, то сразу выходим!
    if (  !(   e.relatedTarget === null || e.relatedTarget.closest('img, :not([svg]')    ) ) return;
   const restore  = e.target.closest('div').querySelector('img');
   const timer = setTimeout(()=> {if (restore) restore.style.opacity = "1"}, 2000);
   const remElem = e.target.closest('div').querySelector(':not(img, .contentDIV)');
   const timer2 = setTimeout(()=> { if (remElem) remElem.remove()},  2000);
    elem = null; // когда точно знаем, что покинули элемент - обнуляемся
};


// блок назначения обработчиков нажания ТАБов и кнопок "+load more"
const ourWorksTabs = document.getElementsByClassName('amazing-work')[0];
ourWorksTabs.addEventListener('click', tabsPressHandler);
loadMoreButton.addEventListener("click", pushMorePicsToPage);
picContainer.addEventListener('mouseover', processContentOnHover);
picContainer.addEventListener('mouseout', deleteContentOnHover);